# ricepuffs

My OpenBSD configuration files. Formerly openrice.

*ricepuffs = rice + pufferfish*

Install them with:

```shell
doas make install
```

Or select an individual configuration file or program:

```shell
doas make dwm # for dwm
doas make neovim # for neovim, requires vim-plug
```

Available targets:

 * `install`: perform all of the below
 * `dwm`: dynamic window manager
 * `st`: simple terminal
 * `dmenu`: dynamic menu
 * `nsxiv`: neo simple X image viewer
 * `xsession`: ~/.xsession
 * `neovim`: ~/.config/nvim/* (requires packer)
 * `packer`: [packer.nvim](https://github.com/wbthomason/packer.nvim)
 * `neofetch`: ~/.config/neofetch/config.conf

## Licence

The builds of dwm, st, dmenu and nsxiv in this repository are licensed under the GNU General Public Licence version 2 **only**.
